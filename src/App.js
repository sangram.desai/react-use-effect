import React, { useState, useEffect } from 'react';
import './App.css';

function App() {

  const [count1, setCount1] = useState(0)
  const [count2, setCount2] = useState(0)

  useEffect(() => {
    console.log("use-effect");
    document.title = `Count1: ${count1}`
  },[count1]);

  function incrementCount1() {
    setCount1(count1 + 1);
  }

  function incrementCount2() {
    setCount2(count2 + 1);
  }

  return (
    <div className="App">
      <h3>count1 :{count1}</h3>
      <button type="button" onClick={incrementCount1}>Increment Count1</button>

      <h3>count2 :{count2}</h3>
      <button type="button" onClick={incrementCount2}>Increment Count2</button>
    </div>
  );
}

export default App;
